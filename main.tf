locals {
  network          = "vpc-retya" # Network name
  region           = "us-east1" # us-east1
  project_id       = "retya-projects" # GCP Project ID
  subnetwork       = "sub-retya" # Subnetwork name
}

// Configure the Google Cloud provider
provider "google" {
 credentials = file("CREDENTIALS_FILE.json")
 project     = local.project_id
 region      = local.region
}

module "gke" {
  source           = "./modules/gke"
  cluster          = "retya-cluster" # Cluster Name
  network          = local.network
  project          = local.project_id
  region           = local.region
  subnetwork       = local.subnetwork
  zones            = ["us-east1-b", "us-east1-c", "us-east1-d"]
}

provider "google-beta" {
  credentials = file("CREDENTIALS_FILE.json")
  project     = local.project_id
  region      = local.region
}


module "vpc" {
  source           = "./modules/vpc"
  project          = local.project_id
  network          = local.network
  region           = local.region
  subnetwork       = local.subnetwork
}
